<?php
namespace App;

use App\Models\Team;
use Illuminate\View\View;

class TeamPointsComposer
{
    public function __construct(\App\Models\Teams\Repository $teams)
    {
        $this->teams = $teams;
    }

    public function create(View $view)
    {
        $view->with('points', $this->teams->points(Team::first()));
    }
}