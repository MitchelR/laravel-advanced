@extends('template')

@section('content')
<form action="{{ route('teams.index') }}" method="POST">
    @csrf
    @inputTextBox('title')
    <button type="submit" class="btn btn-primary">Create</button>
</form>
<p>Team {{ $points }}</p>
@endsection